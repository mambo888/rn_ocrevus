import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    TextInput,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
    saveData,
    getData,
    deleteOneData,
    getAllData,
    deleteAllData,
} from '@utils/GlobalFunction';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import RectShadowImage from '@component/RectShadowImage';
const ViewWidth = Math.round(Global.ScreenWidth * 0.7);
const ViewLeft = Math.round((Global.ScreenWidth - ViewWidth) / 2);
const ItemImages = [
    require('@images/inject.png'),
    require('@images/flask.png'),
    require('@images/plus.png'),
];
const C_BACK_ADDDESCRIPTION = '#8C95BF';

export default class AddNewView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stage: 1, // 1~2
            typeOfStage2: 0, // 1~3
        };
    }

    pressClose() {
        this.props.onClose();
    }

    pressItem(index) {
        // 1~3
        this.setState({
            stage: 2,
            typeOfStage2: index,
        });
    }

    renderAddNew() {
        // const stage = this.state.stage;
        return (
            <View style={styles.addnew_container}>
                <View style={styles.addnew_closewrapper}>
                    <TouchableOpacity
                        onPress={this.pressClose.bind(this)}
                        style={styles.addnew_close}>
                        <AntDesignIcon name="close" size={15} color={'white'} />
                    </TouchableOpacity>
                </View>

                <View style={styles.addnew_desc}>
                    <TextS1
                        family="reg"
                        size="15"
                        color={'white'}
                        label={'Welchen Termin möchtest du eintragen?'}
                        textstyle={{ textAlign: 'center' }}
                    />
                </View>

                <View style={styles.addnew_threeitems}>
                    <View style={styles.addnew_threeitems_wrapper}>
                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[0]}
                                onPress={this.pressItem.bind(this, 1)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Impfung'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>

                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[1]}
                                onPress={this.pressItem.bind(this, 2)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Blutunter-suchung'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>

                        <View style={styles.addnew_oneitem}>
                            <RectShadowImage
                                width={'100%'}
                                image={ItemImages[2]}
                                onPress={this.pressItem.bind(this, 3)}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Anderer Termin'}
                                textstyle={styles.addnew_oneitemtext}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    renderAddNewProperty() {
        const type = this.state.typeOfStage2;
        if (type === 3) {
            return (
                <View style={styles.addblood_wrapper}>
                    <View style={styles.addblood_closewrapper}>
                        <TouchableOpacity
                            onPress={this.pressClose.bind(this)}
                            style={styles.addblood_close}>
                            <AntDesignIcon name="close" size={15} color={'white'} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.add_titleinput}>
                        <TextInput
                            autoCorrect={false}
                            autoCapitalize={'sentences'}
                            underlineColorAndroid="transparent"
                            multiline={false}
                            maxLength={50}
                            textAlignVertical="bottom"
                            style={styles.addother_titleinputtext}
                            onChangeText={text => this.setState({ title: text })}
                            value={this.state.title}
                            placeholder="Welcher Termin?"
                            placeholderTextColor="white"
                        />
                    </View>

                    <View style={styles.addother_dateselector}>
                        <View style={styles.addblood_dateselectorwrapper}>
                            <TouchableOpacity style={styles.addblood_dateselector_date}>
                                <TextS0
                                    family="reg"
                                    size="12"
                                    color={'white'}
                                    label={'Heute'}
                                    textstyle={styles.mr10}
                                />
                                <AntDesignIcon name="caretdown" size={10} color={'white'} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.addblood_dateselector_time}>
                                <TextS0
                                    family="reg"
                                    size="12"
                                    color={'white'}
                                    label={'08:00 Uhr'}
                                    textstyle={styles.mr10}
                                />
                                <AntDesignIcon name="caretdown" size={10} color={'white'} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.addother_descinputwrapper}>
                        <View style={styles.addother_descsubwrapper}>
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={'Beschreibung'}
                                textstyle={styles.mt20}
                            />
                            <TextInput
                                autoCorrect={false}
                                multiline={true}
                                numberOfLines={3}
                                autoCapitalize={'sentences'}
                                underlineColorAndroid="transparent"
                                multiline={false}
                                maxLength={150}
                                textAlignVertical="bottom"
                                style={styles.addother_descinput}
                                onChangeText={text => this.setState({ title: text })}
                                value={this.state.title}
                                placeholder=""
                            />
                        </View>
                    </View>

                    <View style={styles.addother_savewrapper}>
                        <TouchableOpacity style={styles.addother_save}>
                            <TextS0
                                family="demi"
                                size="12"
                                color={'white'}
                                label={'SPEICHERN'}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else {
            // type 1,2
            return (
                <View style={styles.addblood_wrapper}>
                    <View style={styles.addblood_closewrapper}>
                        <TouchableOpacity
                            onPress={this.pressClose.bind(this)}
                            style={styles.addblood_close}>
                            <AntDesignIcon name="close" size={15} color={'white'} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.add_titleinput}>
                        <TextS0
                            family="reg"
                            size="18"
                            color={'white'}
                            label={'Blutuntersuchung'}
                        />
                    </View>

                    {/* date selection */}
                    <View style={styles.addblood_dateselector}>
                        <View style={styles.addblood_dateselectorwrapper}>
                            <TouchableOpacity style={styles.addblood_dateselector_date}>
                                <TextS0
                                    family="reg"
                                    size="12"
                                    color={'white'}
                                    label={'10 NOV 2019'}
                                    textstyle={styles.mr10}
                                />
                                <AntDesignIcon name="caretdown" size={10} color={'white'} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.addblood_dateselector_time}>
                                <TextS0
                                    family="reg"
                                    size="12"
                                    color={'white'}
                                    label={'08:30 AM'}
                                    textstyle={styles.mr10}
                                />
                                <AntDesignIcon name="caretdown" size={10} color={'white'} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* image content */}
                    <View style={styles.addblood_imagewrapper}>
                        <View style={styles.addblood_imagesubwrapper}>
                            <RectShadowImage
                                width={'30%'}
                                image={ItemImages[1]}
                                backcolor={Global.C_GRAYBACK}
                            />
                            <TextS0
                                family="reg"
                                size="12"
                                color={'white'}
                                label={
                                    'Fülle die Checkliste für die Blutuntersuchung bitte zusammen mit Deinem Arzt aus'
                                }
                                textstyle={styles.addblood_imagetext}
                            />
                        </View>
                    </View>

                    {/* delete save */}
                    <View style={styles.addblood_buttonwrapper}>
                        <TouchableOpacity style={styles.addblood_delete}>
                            <TextS0
                                family="demi"
                                size="10"
                                color={'white'}
                                label={'LÖSCHEN'}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.addblood_save}>
                            <TextS0
                                family="demi"
                                size="10"
                                color={'white'}
                                label={'SPEICHERN'}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }

    render() {
        const stage = this.state.stage;
        const isToday = this.props.isToday;

        let ViewTop = 0;
        if (isToday) {
            ViewTop = Math.round(Global.ScreenHeight * 0.39) + 60;
        } else {
            ViewTop = Math.round(Global.ScreenHeight * 0.31) + 60;
        }

        return (
            <View style={styles.container}>
                <View style={[styles.modalcontainer, { top: ViewTop }]}>
                    {stage === 1 ? this.renderAddNew() : this.renderAddNewProperty()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        zIndex: 20,
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        backgroundColor: 'transparent',
        alignItems: 'center',
    },
    modalcontainer: {
        position: 'absolute',
        width: ViewWidth,
        aspectRatio: 1,
        left: ViewLeft,
        backgroundColor: Global.C_PERIWINKLE,
        borderRadius: 5,
    },
    addnew_container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addnew_closewrapper: {
        flex: 0.15,
        width: '100%',
        alignItems: 'flex-end',
    },
    addnew_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_desc: {
        flex: 0.3,
        width: '80%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    addnew_threeitems: {
        flex: 0.55,
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addnew_threeitems_wrapper: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addnew_oneitem: {
        width: '30%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    addnew_oneitemtext: {
        marginTop: 10,
        textAlign: 'center',
    },
    addblood_wrapper: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    addblood_closewrapper: {
        flex: 0.15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    addblood_close: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    add_titleinput: {
        flex: 0.15,
        marginHorizontal: 30,
    },
    addblood_dateselector: {
        flex: 0.1,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'flex-end',
    },
    addblood_dateselectorwrapper: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    addblood_dateselector_date: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    addblood_dateselector_time: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
    },
    mr10: {
        marginRight: 10,
    },
    addother_dateselector: {
        flex: 0.13,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'flex-end',
    },
    addblood_imagewrapper: {
        flex: 0.46,
        marginHorizontal: 20,
    },
    addblood_imagesubwrapper: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row',
    },
    addblood_imagetext: {
        marginLeft: 15,
        width: '60%',
    },
    addblood_buttonwrapper: {
        flex: 0.14,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    addblood_delete: {
        marginRight: 10,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addblood_save: {
        marginRight: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    addother_titleinputtext: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 18,
        fontWeight: 'normal',
        color: 'white',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
    },
    addother_descinputwrapper: {
        flex: 0.43,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    addother_descsubwrapper: {
        width: '100%',
        height: '80%',
        backgroundColor: C_BACK_ADDDESCRIPTION,
        borderRadius: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    mt20: {
        marginTop: 20,
    },
    addother_descinput: {
        fontFamily: Global.F_Avenir_Reg,
        fontSize: 12,
        fontWeight: 'normal',
        color: 'white',
    },
    addother_savewrapper: {
        flex: 0.14,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    addother_save: {
        marginRight: 30,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
});
